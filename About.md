---
title: "Exploratory data analysis"
author: "AY"
date: today
format: 
  html:
    toc: true
    toc-depth: 4
    toc-expand: false
    number-sections: true
    number-depth: 4
editor: source
editor_options: 
  chunk_output_type: console
bibliography: references.bib
qknitr:
  opts_chunk:
    comment: "#>"
    collapse: true
execute: 
  eval: true
---

# Introduction

This chapter will show how to use visualization and transformation to explore your data in a systematic way, a task that statisticians call exploratory data analysis, or EDA for short. EDA is an iterative cycle. You:

1.  Generate questions about your data.
2.  Search for answers by visualizing, transforming, and modelling your data.
3.  Use what you learn to refine your questions and/or generate new questions.

EDA is not a formal process with a strict set of rules. More than anything, EDA is a state of mind. During the initial phases of EDA you should feel free to investigate every idea that occurs to you. Some of these ideas will pan out, and some will be dead ends. As your exploration continues, you will home in on a few particularly productive insights that you'll eventually write up and communicate to others.

EDA is an important part of any data analysis, even if the primary research questions are handed to you on a platter, because you always need to investigate the quality of your data. Data cleaning is just one application of EDA: you ask questions about whether your data meets your expectations or not. To do data cleaning, you'll need to deploy all the tools of EDA: visualization, transformation, and modelling.

```{r}
library(tidyverse)
library(patchwork)
```

## Exercises

1.  Explore the distribution of each of the x, y, and z variables in diamonds. What do you learn? Think about a diamond and how you might decide which dimension is the length, width, and depth.

```{r}
p1 <- ggplot(diamonds, aes(x = x)) + 
  geom_histogram(binwidth = 0.5)

p2 <- ggplot(diamonds, aes(x = y)) + 
  geom_histogram(binwidth = 0.5)

p3 <- ggplot(diamonds, aes(x = z)) + 
  geom_histogram(binwidth = 0.5)

p1 + p2 + p3
```

